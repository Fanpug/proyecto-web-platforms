FROM node
LABEL authors = "Alejandro Diaz, Humberto Navarro, Adan Ramirez"
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start