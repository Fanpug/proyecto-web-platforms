const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _name: String,
    _role: String,
    _functionality: String,
    _benefit: String,
    _priority: String,
    _size: String,
    _nameAccept: String,
    _context: String,
    _event: String,
    _result: String,

});

class Story
{
    constructor(name, role, functionality, benefit, priority, size, nameAccept, context, event, result)
    {
        this._name = name;
        this._role = role;
        this._functionality = functionality;
        this._benefit = benefit;
        this._priority = priority;
        this._size = size;
        this._nameAccept = nameAccept;
        this._context = context;
        this._event = event;
        this._result = result;
    }

    get name()
    {
        return this._name;
    }

    set name(value)
    {
        this._name = value;
    }

    get role()
    {
        return this._role;
    }

    set role(value)
    {
        this._role = value;
    }

    get functionality()
    {
        return this._functionality;
    }

    set functionality(value)
    {
        this._functionality = value;
    }

    get benefit()
    {
        return this._benefit;
    }

    set benefit(value)
    {
        this._benefit = value;
    }

    get priority()
    {
        return this._priority;
    }

    set priority(value)
    {
        this._priority = value;
    }

    get size()
    {
        return this._size;
    }

    set size(value)
    {
        this._size = value;
    }

    get nameAccept()
    {
        return this._nameAccept;
    }

    set nameAccept(value)
    {
        this._nameAccept = value;
    }

    get context()
    {
        return this._context;
    }

    set context(value)
    {
        this._context = value;
    }

    get event()
    {
        return this._event;
    }

    set event(value)
    {
        this._event = value;
    }

    get result()
    {
        return this._result;
    }

    set result(value)
    {
        this._result = value;
    }

}

schema.loadClass(Story);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Story', schema);
