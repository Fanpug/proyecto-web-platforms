const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _name:String,
    _lastName:String,
    _birthdate: Date,
    _curp: String,
    _rfc: String,
    _address: String,
    _skillList: [{type: mongoose.Schema.ObjectId, ref: 'Skill'}],
    _email:{type: String, unique: true},
    _password:{type: String, select: false},
    _salt: String
});

class User {
    constructor(name, lastName, birthdate, curp, rfc, address, skillList, email, password, salt){
        this._name = name;
        this._lastName = lastName;
        this._birthdate = birthdate;
        this._curp = curp;
        this._rfc = rfc;
        this._address = address;
        this._skillList = skillList;
        this._email = email;
        this._password = password;
        this._salt = salt;

    }

    get name(){
        return this._name;
    }

    set name(value){
        this._name = value;
    }

    get lastName(){
        return this._lastName;
    }

    set lastName(value){
        this._lastName = value;
    }

    get birthdate(){
        return this._birthdate;
    }

    set birthdate(value){
        this._birthdate = value;
    }

    get curp(){
        return this._curp;
    }

    set curp(value){
        this._curp = value;
    }

    get address(){
        return this._address;
    }

    set address(value){
        this._address = value;
    }

    get skillList(){
        return this._skillList;
    }

    set address(value){
        this._skillList = value;
    }

    get rfc(){
        return this._rfc;
    }

    set rfc(value){
        this._rfc = value;
    }

    get email(){
        return this._email;
    }

    set email(value){
        this._email = value;
    }

    get password(){
        return this._password;
    }

    set password(value){
        this._password = value;
    }

    get salt(){
        return this._salt;
    }

    set salt(value){
        this._salt = value;
    }
}

schema.loadClass(User);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', schema);
