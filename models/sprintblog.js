const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _sprint: String
});

class Sprintblog
{
    constructor(sprint)
    {
        this._sprint = sprint;
    }

    get sprint()
    {
        return this._sprint;
    }

    set sprint(value)
    {
        this._sprint = value;
    }

}

schema.loadClass(Sprintblog);
module.exports = mongoose.model('Sprintblog', schema);