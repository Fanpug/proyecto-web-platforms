const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name:String,
    _level: String
});

class Skill
{
    constructor(name, level)
    {
        this._name = name;
        this._level = level;
    }
    
    get name()
    {
        return this._name;
    }
    
    set name(value)
    {
        this._name = value;
    }

    get level()
    {
        return this._level;
    }

    set level(value)
    {
        this._level = value;
    }
}

schema.loadClass(Skill);
module.exports = mongoose.model('Skill', schema);