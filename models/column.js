const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _productBlog: String,
    _releaseBlog: String//[{type: mongoose.Schema.ObjectId, ref: 'Sprintblog'}]
});

class Column
{
    constructor(productBlog, releaseBlog)
    {
        this._productBlog = productBlog;
        this._releaseBlog = releaseBlog;
    }

    get productBlog()
    {
        return this._productBlog;
    }

    set productBlog(value)
    {
        this._productBlog = value;
    }

    get releaseBlog()
    {
        return this._releaseBlog;
    }

    set releaseBlog(value)
    {
        this._releaseBlog = value;
    }

}

schema.loadClass(Column);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Column', schema);
