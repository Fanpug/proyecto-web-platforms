const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const user = require('./user');

const schema = mongoose.Schema({
    _name: String,
    _solicitedDate: Date,
    _startDate: Date,
    _description: String,
    _projectManager: [{type: mongoose.Schema.ObjectId, ref: 'User'}],
    _productOwner: String,
    _debteam: [{type: mongoose.Schema.ObjectId, ref: 'User'}]
});

class Project
{
    constructor(name, solicitedDate, startDate, description, projectManager, productOwner, debteam)
    {
        this._name = name;
        this._solicitedDate = solicitedDate;
        this._startDate = startDate;
        this._description = description;
        this._projectManager = projectManager;
        this._productOwner = productOwner;
        this._debteam = debteam;
    }

    get name()
    {
        return this._name;
    }

    set name(value)
    {
        this._name = value;
    }

    get solicitedDate()
    {
        return this._solicitedDate;
    }

    set solicitedDate(value)
    {
        this._solicitedDate = value;
    }

    get startDate()
    {
        return this._startDate;
    }

    set startDate(value)
    {
        this._startDate = value;
    }

    get description()
    {
        return this._description;
    }

    set description(value)
    {
        this._description = value;
    }

    get projectManager()
    {
        return this._projectManager;
    }

    set projectManager(value)
    {
        this._projectManager = value;
    }

    get productOwner()
    {
        return this._productOwner;
    }

    set productOwner(value)
    {
        this._productOwner = value;
    }

    get debteam()
    {
        return this._debteam;
    }

    set debteam(value)
    {
        this._debteam = value;
    }

}

schema.loadClass(Project);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Project', schema);
