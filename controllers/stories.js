const express = require('express');
const async = require('async');
const Story = require('../models/story');

function list(req, res, next) {
    const page = req.params.page ? req.params.page : 1;
    Story.paginate({},{
      page: page,
      limit:100
    }).then(stories => res.status(200).json({
      message: res.__('ok.find'),
      objs: stories
    })).catch(error => res.status(500).json({
      message: res.__('bad.find'),
      obj:error
    }));
}

function index(req, res, next){
    let id = req.params.id;
    Story.findOne({_id: id}).then(user => res.status(200).json({
      message: res.__('ok.find'),
      objs: story
    })).catch(error => res.status(500).json({
      message: res.__('bad.find'),
      obj: error
    }));
}

function create(req, res, next){
    let story = new Story({
        _name: req.body.name,
        _role: req.body.role,
        _functionality: req.body.functionality,
        _benefit: req.body.benefit,
        _priority: req.body.priority,
        _size: req.body.size,
        _nameAccept: req.body.nameAccept,
        _context: req.body.context,
        _event: req.body.event,
        _result: req.body.result,
    });

    story.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(error => res.status(500).json({
        message: res.__('bad.create'),
        obj: error
    }));
}

function replace(req,res){
  let id = req.params.id;
  let story = new Object();

  if(req.body.name){
    story._name = req.body.name;
  }
  if(req.body.role){
    story._role = req.body.role;
  }
  if(req.body.functionality){
    story._functionality = req.body.functionality;
  }
  if(req.body.benefit){
    story._benefit = req.body.benefit;
  }
  if(req.body.size){
    story._size = req.body.size;
  }
  if(req.body.nameAccept){
    story._nameAccept = req.body.nameAccept;
  }
  if(req.body.context){
    story._context = req.body.context;
  }
  if(req.body.event){
    story._event = req.body.event;
  }
  if(req.body.result){
    story._result = req.body.result;
  }

  Story.findOneAndUpdate({_id: id}, story).then(story => res.status(200).json({
    message: res.__('ok.edit'),
    obj: error
  })).catch(error => res.staus(500).json({
    message: res.__('bad.edit'),
    obj: error
  }));
}

function destroy(req, res, next){
    const id= req.params.id;
    Story.deleteOne({_id: id}).then(story => res.status(200).json({
      message: res.__('ok.destroy'),
      obj:  error
    })).catch(error => res.status(500).json({
      message: res.__('bad.destroy'),
      obj:error
    }));
}


module.exports = {
    list, index, create, replace, destroy
}
