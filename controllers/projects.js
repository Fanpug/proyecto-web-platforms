const express = require('express');
const Project = require('../models/project');


function list(req, res, next) {
    const page = req.params.page ? req.params.page : 1;
    Project.paginate({}, {
        page: page,
        limit: 100
    }).then(projects => res.status(200).json({
        message: res.__('ok.find'),
        objs: projects
    })).catch(error => res.status(500).json({
        message: res.__('bad.find'),
        obj: error
    }));
}

function index(req, res, next){
    let id = req.params.id;
    Project.findOne({_id: id}).then(project => res.status(200).json({
        message: res.__('ok.find'),
        obj: project
    })).catch(error => res.status(500).json({
        message: res.__('bad.find'),
        obj: error
    }));
}

function create(req, res, next){
    let project = new Project({
        _name: req.body.name,
        _solicitedDate: req.body.solicitedDate,
        _startDate: req.body.startDate,
        _description: req.body.description,
        _projectManager: req.body.projectManager,
        _productOwner: req.body.productOwner,
        _debteam: req.body.debteam
    });

    project.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(error => res.status(500).json({
        message: res.__('bad.create'),
        obj: error
    }));
}

function replace(req, res, next){
    let id = req.params.id;

    let project = new Object();

    if(req.body.name){
        project._name = req.body.name;
    }
    if(req.body._solicitedDate){
        project._solicitedDate = req.body.solicitedDate;
    }
    if(req.body._startDate){
        project._startDate = req.body.startDate;
    }
    if(req.body._description){
        project._description = req.body.description;
    }
    if(req.body._projectManager){
        project._projectManager = req.body.projectManager;
    }
    if(req.body._productOwner){
        project._productOwner = req.body.productOwner;
    }
    if(req.body._debteam){
        project._debteam = req.body.debteam;
    }

    project.findOneAndUpdate(
        {_id: id},
        project
    ).then(project => res.status(200).json({
        message: res.__('ok.edit'),
        objs: project
    })).catch(error=> res.status(500).json({
        message: res.__('bad.edit'),
        obj: error
    }));

}

function destroy(req, res, next){
    const id = req.params.id;
    Project.deleteOne({_id: id}).then(project => res.status(200).json({
        message: res.__('ok.destroy'),
        objs: project
    })).catch(error => res.status(500).json({
        message: res.__('bad.destroy'),
        obj: error
    }));
}


module.exports = {
    list, index, create, replace, destroy
}
