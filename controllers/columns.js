const async = require('async');
const brypt = require('bcrypt');
const Column = require('../models/column');

function list(req, res, next){
  const page = req.params.page ? req.params.page : 1;
  Column.paginate({},{
    page: page,
    limit: 100
  }).then(columns => res.status(200).json({
    message: res.__('ok.find'),
    objs: columns
  })).catch(error => res.status(500).json({
    message: res.__('bad.find'),
    obj: error
  }));
}

function index(req,res){
  let id = req.params.name;
  Column.findOne({_id: id}).then(column => res.status(200).json({
    message: res.__('ok.find'),
    objs: column
  })).catch(error => res.status(500).json({
    message: res.__('bad.find'),
    obj: error
  }));
}

function create(req,res){
  let column = new Column({
    _productBlog: req.body.productBlog,
    _releaseBlog: req.body.releaseBlog
  });

  column.save().then(obj => res.status(200).json({
    message: res.__('ok.create'),
    obj: obj
  })).catch(error => res.status(500).json({
    message: res.__('bad.create'),
    obj: error
  }));
}

function replace(req, res){
  let id = req.params.id;

  let column = new Object();

  if(req.body.name){
    column._na = req.body.name;
  }
  if(req.body.projectId){
  column._projectId = req.body.releaseBlog;
  }

  column.findOneAndUpdate(
    {_id : id},
    column
  ).then(column => res.status(200).json({
    message: res._('ok.edit'),
    objs: column
  })).catch(error => res.status(500).json({
    message: res._('bad.edit'),
    obj: error
  }));
}

function destroy(req,res){
  const id = req.params.id;
  Column.deleteOne({_id: id}).then(column => res.status(200).json({
    message: res.__('ok.destroy'),
    objs: column
  })).catch(error => res.status(500).json({
    message: res.__('bad.destroy'),
    obj: error
  }));
}

module.exports = {
  list,
  index,
  create,
  replace,
  destroy
}
