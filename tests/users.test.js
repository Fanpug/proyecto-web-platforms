const supertest = require('supertest');

const app = require('../app');

var key = "";

describe('testing authentication system', ()=>{
  it('Deberia de obtener un login con usuario y cotraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email': 'adan@uach.mx', 'password':'312730'})
    .expect(200)
    .end(function(err,res){
      key = res.body.obj;
      done();
    })
  });
});

describe('Testing user routes', ()=>{
  it('Deberia tener la lista de usuarios', (done)=>{
    supertest(app).get('/users/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err,res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});
