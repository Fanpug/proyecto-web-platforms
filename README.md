# Proyecto reto I: Desarrollar un manejador de proyectos

Este repositorio fue creado para el proyecto semestral de la materia de Web Platforms que se actualizara conforme avanza el semestre.

## Descripción de los problemas

El siguiente proyecto reto esta basado en la implementación de un manejado de proyectos de software basado en la metodología scrum.

Una empresa de desarrollo de software quiere llevar de mejor manera el control de sus proyectos, por eso ha solicitado cotizaciones a diferentes empresas consultoras externas para desarrollar una plataforma acorde a sus necesidades.

### Diagrama de clases

![](./images/diagrama.png)

### Diagrama de secuencia

![](./images/diagramaSecuencia.png)

## Prerequisitos

Los prerequisitos para construir el proyecto son:
-Git
-Nodejs
-Docker
-Mongo
-NPM
-Editor de texto
-Terminal/Consola

### Instalando

Para instalar o descargar nuestro proyecto se deben seguir los siguientes pasos:

1. Clonar nuestro repositorio; esto puede realizarse manualmente desde este repositorio o desde la consola del sistema operativo con el comando:

`git clone https://gitlab.com/Fanpug/proyecto-web-platforms/`

2. Una vez clonado el proyecto, se deben instalar las dependencias de librerias NPM necesarias para que el programa pueda funcionar de manera correcta con el comando:

`npm install`

3. Una vez instaladas las dependencias, simplemente se debe levantar la aplicacion con el comando:

`npm start`

## Corriendo pruebas

Para correr pruebas de nuestra aplicacion, simplemente se necesita ejecutar el comando

`npm test`

y se correran pruebas predeterminadas para determinar la funcionalidad de la aplicacion.

## Link hacia la app en Heroku

https://fast-everglades-31348.herokuapp.com/

## Construido con

- Visual Studio Code - Editor de texto
- JavaScript - Lenguaje de programación interpretado
- Node
- Express
- mongodb

## Contribuiciones

No aceptamos contribuciones >:|

## Autores

- **Adan Alejandro Ramirez Quintero (312730)** - [Gitlab](https://gitlab.com/a312730)
- **Humberto Alejandro Navarro Andujo (320669)** - [Gitlab](https://gitlab.com/Fanpug)
- **Raul Alejandro Diaz Gutierrez (329782)** - [Gitlab](https://gitlab.com/a329782)

## Licencia

El proyecto tiene libre licencia o algo asi ;)

## Agradecimientos

El profesor Luis Antonio Ramirez Martinez fue quien nos inspiro a realizar este proyecto/tarea, muchas gracias profe! :)

Gracias a las personas de PurpleBooth por su plantilla/referencia de como debe ser el [formato de un readme](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2).
